*Calm Your Alerts* changes the behavior of the destruction alarm. Instead of destroying your concentration, it'll only sound once every so often, with subsequent periodic alerts at a lower volume.

Check out the in-game settings for a bespoke experience.

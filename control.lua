local handler = require "event_handler"


local handle_alert = function (tick, player)
    local pidx = player.index

    local alerts = global.alerts[pidx]
    local rs = settings.get_player_settings (pidx)
    local delay_ticks = rs["time-between-alerts"].value * 60

    if not alerts then
        global.alerts[pidx] = {
            last_alert_tick = 0,
            last_death_tick = 0,
        }
        alerts = global.alerts[pidx]
    end

    if alerts then
        local delta_alert_ticks = tick - alerts.last_alert_tick
        local delta_death_ticks = tick - alerts.last_death_tick

        if delta_death_ticks >= delay_ticks then
            -- We've not received alerts within the time period,
            -- we want to use the this next alert at the normal volume.
            player.play_sound {
                path="cya-alert-destroyed",
                volume_modifier=1.0
            }
            alerts.last_alert_tick = tick

        elseif delta_alert_ticks >= delay_ticks then
            -- We've had some deaths within this time period, so
            -- play a lower volume sound.
            player.play_sound {
                path="cya-alert-destroyed",
                volume_modifier=rs["alert-volume-modifier"].value
            }
            alerts.last_alert_tick = tick
        end

        alerts.last_death_tick = tick
    end
end


local on_entity_died = function (event)
    local entity = event.entity
    if entity and entity.valid then
        if entity.type == "combat-robot" then
            return
        end

        local caused = event.force
        local force = entity.force

        if caused ~= force then
            for _, player in pairs (force.players) do
                if player.valid then
                    handle_alert (event.tick, player)
                end
            end
        end
    end
end



local lib = {}


lib.events = {
    [defines.events.on_entity_died] = on_entity_died,
}


lib.event_filters = {
    [defines.events.on_entity_died] = {{filter="type", type="unit", invert=true }},
}


lib.set_event_filters = function (event_filters)
    for event, filters in pairs (event_filters) do
        script.set_event_filter (event, filters)
    end
end


-- Called on new game.
-- Called when added to exiting game.
lib.on_init = function()
    global.alerts = {}
    lib.set_event_filters (lib.event_filters)
end


-- Not called when added to existing game.
-- Called when loaded after saved in existing game.
lib.on_load = function()
    lib.set_event_filters (lib.event_filters)
end


-- Not called on new game.
-- Called when added to existing game.
lib.on_configuration_changed = function (config)
    global.alerts = {}
end


-- Register the events.
handler.add_lib (lib)
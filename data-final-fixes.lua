local function add_utility_sound (name, filename)
  data.raw["utility-sounds"]["default"][name] =
  {
    {
      filename = filename
    }
  }
end

-- More like replace.
add_utility_sound ("alert_destroyed","__core__/sound/silence-1sec.ogg")

-- Add our sound prototype so it can be called.
data:extend ({
  {
    type = "sound",
    name = "cya-alert-destroyed",
    filename = "__core__/sound/alert-destroyed.ogg",
    category = "alert"
  }
})

data:extend ({
    {
        type = "double-setting",
        name = "time-between-alerts",
        setting_type = "runtime-per-user",
        default_value = 20,
        minimum_value = 1,
        order = "a"
    },
    {
        type = "double-setting",
        name = "alert-volume-modifier",
        setting_type = "runtime-per-user",
        default_value = 0.2,
        minimum_value = 0,
        maximum_value = 1.0,
        order = "ba"
    },
})
